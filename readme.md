A [libli](https://www.npmjs.com/package/@sctlib/libli) instance,
pinned to `#berlin-insoumise:matrix.org`, to display and organize the
public data available in this [matrix](https://matrix.org/) space.

# Usage

To edit and cooperate on the pages (rooms), best use a [matrix client
](https://matrix.to/#/#berlin-insoumise:matrix.org) (such as element).

# Development

The [npm module `serve`](https://www.npmjs.com/package/serve) can be used to run a local development server.

Use the command: `npx serve -s .` (you will need
[npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm))
when inside the project's root folder; `-s` for letting the
"javascript web app" handle the URLs (and not serve).

## Depedencies

### `@sctlib/libli`

Docs: https://gitlab.com/sctlib/libli

### `@sctlib/matrix-room-element`

Docs: https://gitlab.com/sctlib/matrix-room-element
